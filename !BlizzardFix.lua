if (UIDROPDOWNMENU_VALUE_PATCH_VERSION or 0) < 3 then
	UIDROPDOWNMENU_VALUE_PATCH_VERSION = 3
	hooksecurefunc("UIDropDownMenu_InitializeHelper", function()
		if UIDROPDOWNMENU_VALUE_PATCH_VERSION ~= 3 then
			return
		end
		for i=1, UIDROPDOWNMENU_MAXLEVELS do
			for j=1+_G["DropDownList" .. i].numButtons, UIDROPDOWNMENU_MAXBUTTONS do
				local b = _G["DropDownList" .. i .. "Button" .. j]
				if not issecurevariable(b, "value") then
					b.value = nil
					repeat
						j, b["fx" .. j] = j+1
					until issecurevariable(b, "value")
				end
			end
		end
	end)
end