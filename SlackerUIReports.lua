SlackerUI.Reports = {}
SlackerUI.Reports.ReportData = nil
SlackerUI.Reports.ReportAvailable = nil
SlackerUI.Reports.GenerateReport = nil

function SlackerUI.Reports.ReportAvailable(typ, id)
	for i=1,#SlackerUI.Reports.ReportData,1
	do
		local report = SlackerUI.Reports.ReportData[i]
		if (typ==report["type"] and id==report["id"])
		then
			return true
		end
	end
	return false
end

function SlackerUI.Reports.GenerateReport(typ, id, data)
	for i=1,#SlackerUI.Reports.ReportData,1
	do
		local report = SlackerUI.Reports.ReportData[i]
		if (report.type==typ and report.id==id)
		then
			local report = SlackerUI.Reports.ReportData[i]
			report.func(data)
			return
		end
	end
	SlackerHelper.error("No report found.")
end

local function ShowReport(title, cols, data, btn1, btn2)
	local f = CreateFrame("Frame", nil, UIParent, "SlackerUI_ReportTemplate")
	f.Button1:Hide()
	f.Button2:Hide()
	f.Title:SetText(title)
	local ScrollingTable = LibStub("ScrollingTable");
	local tbl = ScrollingTable:CreateST(cols, 30, 18, nil, f.Table)
	tbl:SetData(data);
	tbl:Refresh();
	tbl:Show();
	local width = math.max(tbl.frame:GetWidth()+40, 200)
	if btn1 ~= nil
	then
		f.Button1:SetText(btn1.text)
		f.Button1:SetScript("OnClick", btn1.callback)
		f.Button1:Show()
	end
	if btn2 ~= nil
	then
		f.Button2:SetText(btn1.text)
		f.Button2:SetScript("OnClick", btn1.callback)
		f.Button2:Show()
	end
	f:SetWidth(width)
	f:Show()
end

local function AwardButton(datatable, indexname, indexaward, func, reason)
	return  {
		text="Award", 
		callback=function()
			SlackerUI.ConfirmDialog("Award the players?", 
			function() 
				for i=1,#datatable,1
				do
					local player = datatable[i]["cols"][indexname]["value"]
					local amount = datatable[i]["cols"][indexaward]["value"]
					if amount ~= nil and amount ~= 0
					then
						SlackerHelper.eval(func, {player=player, amount=amount, message=reason})
					end
				end
			end,
			"Continue",
			"Cancel"
			)
		end
	}
end

local function DungeonMissingBuffs(data)
	local cols = { 
		{
			["name"] = "Ind",
			["width"] = 50,
			["align"] = "LEFT",
			["sort"] = "dsc",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Snapshot",
			["width"] = 150,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Gr",
			["width"] = 20,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "MotW",
			["width"] = 60,
			["align"] = "CENTER",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Intellect",
			["width"] = 60,
			["align"] = "CENTER",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Fortitude",
			["width"] = 60,
			["align"] = "CENTER",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Spirit",
			["width"] = 60,
			["align"] = "CENTER",
			["defaultsort"] = "asc",
		} 
	}
	local datatable = {}
	local i = 0
	for snapshot in data:get_snapshots_iterator()
	do
		local reason = snapshot:get_reason()
		if SlackerHelper.starts_with(reason, "Pull on ") 
		then
			i = i+1
			local snap = {
				["value"] = reason
			}
			local groups = {}
			local classes = {}
			for player in snapshot:get_players_iterator()
			do
				local group = player:get_group()
				local class = player:get_class()
				if groups[group]==nil
				then
					groups[group] = {}
				end
				table.insert(groups[group], player)
				classes[class] = true
			end
			for group,members in pairs(groups)
			do
				local index = {
					["value"] = string.format("%03d.%d",i, group)
				}
				local grp = {
					["value"] = group
				}
				local need =  { ["motw"] = 0, ["int"] = 0, ["stam"] = 0, ["spirit"] = 0 }
				local have =  { ["motw"] = 0, ["int"] = 0, ["stam"] = 0, ["spirit"] = 0 }
				for j=1,#members,1
				do
					local player = members[j]
					local class = player:get_class()
					local online = player:get_online()
					local isCaster = 0
					if SlackerHelper.in_array(class, {"Druid", "Mage", "Hunter", "Priest", "Shaman", "Warlock"}) -- is caster
					then
						isCaster = 1
					end
					if online
					then
						need["motw"] = need["motw"] + 1
						need["stam"] = need["stam"] + 1
						need["int"] = need["int"] + isCaster
						need["spirit"] = need["spirit"] + isCaster
						local buffs = player:get_all_buffs_idonly()
						for k=1,#buffs,1
						do
							local b = buffs[k]
							if b==9885 or b==21850 or b==26991 or b==26990 -- motw
							then
								have["motw"] = have["motw"] + 1
							elseif b==10157 or b==23028 or b==27127 or b==27126 -- int
							then
								have["int"] = have["int"] + isCaster
							elseif b==10938 or b==21564 or b==25389 or b==25392 -- fort
							then
								have["stam"] = have["stam"] + 1
							elseif b==27841 or b==27681 or b==32999 or b==25312 -- spirit
							then
								have["spirit"] = have["spirit"] + isCaster
							end
						end
					end
				end
				local haveClass =  { ["motw"] = classes["Druid"], ["int"] = classes["Mage"], ["stam"] = classes["Priest"], ["spirit"] = classes["Priest"] }
				local text =  { ["motw"] = "0", ["int"] = "0", ["stam"] = "0", ["spirit"] = "0" }
				local color =  { ["motw"] = nil, ["int"] = nil, ["stam"] = nil, ["spirit"] =nil }
				for i,v in ipairs({"motw", "int", "stam", "spirit"})
				do
					if haveClass[v] and 0<need[v]
					then
						text[v] = string.format("%d/%d",have[v],need[v])
						color[v] = { ["r"] = 0.14, ["g"] = 0.53, ["b"] = 0.14, ["a"] = 1.0 } -- green
						if have[v]==0
						then
							color[v] = { ["r"] = 0.82, ["g"] = 0.13, ["b"] = 0.18, ["a"] = 1.0 } -- red
						elseif have[v]<need[v]
						then
							color[v] = { ["r"] = 1.0, ["g"] = 0.75, ["b"] = 0.0, ["a"] = 1.0 } -- amber
						end
					end
				end
				local motw = {
					["value"] = text["motw"],
					["color"] = color["motw"]
				}
				local int = {
					["value"] = text["int"],
					["color"] = color["int"]
				}
				local stam = {
					["value"] = text["stam"],
					["color"] = color["stam"]
				}
				local spirit = {
					["value"] = text["spirit"],
					["color"] = color["spirit"]
				}
				local row = { ["cols"] = {index, snap, grp, motw, int, stam, spirit} }
				table.insert(datatable, row)
			end
		end
	end
	local title = "Missing Buffs for "..data:get_instance_name()
	ShowReport(title, cols, datatable)
end

local function SnapshotWorldBuffs(data)
	local CalculateAward = SlackerHelper.loadstring(SlackerHelper.get_setting("script_report_worldbuff") or "", "script_report_worldbuff")
	local AwardPlayer = SlackerHelper.loadstring(SlackerHelper.get_setting("script_award") or "", "script_award")
	local cols = { 
		{
			["name"] = "Name",
			["width"] = 84,
			["align"] = "LEFT",
			["sort"] = "dsc",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Class",
			["width"] = 60,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Gr",
			["width"] = 20,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Buffs",
			["width"] = 200,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
	}
	if CalculateAward ~= nil
	then
		table.insert(cols, 4,
		{
			["name"] = "Award",
			["width"] = 40,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		})
	end
	local datatable = {}
	local datetime = data:get_datetime()
	local dmfweek = SlackerHelper.is_dmf_week(datetime)
	for player in data:get_players_iterator()
	do
		local name = player:get_name()
		local class = player:get_class()
		local classstr = class
		local group = player:get_group()
		local buffs = player:get_all_buffs_idonly()
		local offlinetext = ""
		local color = SlackerHelper.class_to_color(class)
		if not player:get_online()
		then 
			offlinetext=" (off)" 
			color = SlackerHelper.class_to_color("Offline")
		end
		local buffsfiltered = {}
		for i=1,#buffs,1
		do
			local b = buffs[i]
			local p = SlackerHelper.get_buff_priority(b)
			if SlackerHelper.BuffPriority.WB5<=p and p<=SlackerHelper.BuffPriority.DMF
			then
				table.insert(buffsfiltered, b)
			end
		end
		name = string.format("%s%s",name, offlinetext)
		name = { ["value"] = name, ["color"] = color }
		class = { ["value"] = class, ["color"] = color }
		group = { ["value"] = group }
		buffs = { ["value"] = buffsfiltered }
		local row = { ["cols"] = {name, class, group, buffs} }
		if CalculateAward ~= nil
		then
			local awrd = { ["value"] = SlackerHelper.eval(CalculateAward, {class=classstr, buffs=buffsfiltered, timestamp=datetime, is_dmfweek=dmfweek}) }
			table.insert(row["cols"], 4, awrd)
		end
		table.insert(datatable, row)
	end
	local title = "World Buffs for "..data:get_reason()
	local btn1 = nil
	if CalculateAward and AwardPlayer
	then
		btn1 = AwardButton(datatable, 1, 4, AwardPlayer, "WorldBuffs")
	end
	ShowReport(title, cols, datatable, btn1)
end

local function SnapshotConsumesClassic(data)
	local CalculateAward = SlackerHelper.loadstring(SlackerHelper.get_setting("script_report_consume") or "", "script_report_consume")
	local AwardPlayer = SlackerHelper.loadstring(SlackerHelper.get_setting("script_award") or "", "script_award")
	local cols = { 
		{
			["name"] = "Name",
			["width"] = 84,
			["align"] = "LEFT",
			["sort"] = "dsc",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Class",
			["width"] = 60,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Gr",
			["width"] = 20,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Flask",
			["width"] = 40,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Zanza",
			["width"] = 40,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Prot",
			["width"] = 60,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Consumes",
			["width"] = 120,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Food",
			["width"] = 40,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
	}
	if CalculateAward ~= nil
	then
		table.insert(cols, 4,
		{
			["name"] = "Award",
			["width"] = 40,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		})
	end
	local datatable = {}
	local datetime = data:get_datetime()
	for player in data:get_players_iterator()
	do
		local name = player:get_name()
		local class = player:get_class()
		local classstr = class
		local group = player:get_group()
		local buffs = player:get_all_buffs_idonly()
		local offlinetext = ""
		local color = SlackerHelper.class_to_color(class)
		local flask = {}
		local zanza = {}
		local prot = {}
		local cons = {}
		local food = {}
		local all = {}
		if not player:get_online()
		then 
			offlinetext=" (off)" 
			color = SlackerHelper.class_to_color("Offline")
		end
		local buffsfiltered = {}
		for i=1,#buffs,1
		do
			
			local b = buffs[i]
			local p = SlackerHelper.get_buff_priority(b)
			if p==SlackerHelper.BuffPriority.FLASK
			then
				table.insert(all, b)
				table.insert(flask, b)
			elseif p==SlackerHelper.BuffPriority.ZANZA
			then
				table.insert(all, b)
				table.insert(zanza, b)
			elseif p==SlackerHelper.BuffPriority.GPROT or p==SlackerHelper.BuffPriority.PROT
			then
				table.insert(all, b)
				table.insert(prot, b)
			elseif p==SlackerHelper.BuffPriority.CONS or p==SlackerHelper.BuffPriority.SCROLL
			then
				table.insert(all, b)
				table.insert(cons, b)
			elseif p==SlackerHelper.BuffPriority.FOOD
			then
				table.insert(all, b)
				table.insert(food, b)
			end
		end
		name = string.format("%s%s",name, offlinetext)
		name = { ["value"] = name, ["color"] = color }
		class = { ["value"] = class, ["color"] = color }
		group = { ["value"] = group }
		flask = {["value"] = flask}
		zanza = {["value"] = zanza}
		prot = {["value"] = prot}
		cons = {["value"] = cons}
		food = {["value"] = food}
		local row = { ["cols"] = {name, class, group, flask, zanza, prot, cons, food} }
		if CalculateAward ~= nil
		then
			local awrd = { ["value"] = SlackerHelper.eval(CalculateAward, {class=classstr, buffs=all, timestamp=datetime}) }
			table.insert(row["cols"], 4, awrd)
		end
		table.insert(datatable, row)
	end
	local title = "Consumes for "..data:get_reason()
	local btn1 = nil
	if CalculateAward and AwardPlayer
	then
		btn1 =AwardButton(datatable, 1, 4, AwardPlayer, "Consumes")
	end
	ShowReport(title, cols, datatable, btn1)
end

local function SnapshotConsumesTBC(data)
	local CalculateAward = SlackerHelper.loadstring(SlackerHelper.get_setting("script_report_consume") or "", "script_report_consume")
	local AwardPlayer = SlackerHelper.loadstring(SlackerHelper.get_setting("script_award") or "", "script_award")
	local cols = { 
		{
			["name"] = "Name",
			["width"] = 84,
			["align"] = "LEFT",
			["sort"] = "dsc",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Class",
			["width"] = 60,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Gr",
			["width"] = 20,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Flask/Elixir",
			["width"] = 120,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Food",
			["width"] = 40,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Prot",
			["width"] = 120,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
	}
	if CalculateAward ~= nil
	then
		table.insert(cols, 4,
		{
			["name"] = "Award",
			["width"] = 40,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		})
	end
	local datatable = {}
	local datetime = data:get_datetime()
	for player in data:get_players_iterator()
	do
		local name = player:get_name()
		local class = player:get_class()
		local classstr = class
		local group = player:get_group()
		local buffs = player:get_all_buffs_idonly()
		local offlinetext = ""
		local color = SlackerHelper.class_to_color(class)
		local flask = {}
		local food = {}
		local prot = {}
		local all = {}
		if not player:get_online()
		then 
			offlinetext=" (off)" 
			color = SlackerHelper.class_to_color("Offline")
		end
		local buffsfiltered = {}
		for i=1,#buffs,1
		do
			
			local b = buffs[i]
			local p = SlackerHelper.get_buff_priority(b)
			if p==SlackerHelper.BuffPriority.FLASK or p==SlackerHelper.BuffPriority.GUARD or p==SlackerHelper.BuffPriority.BATTLE
			then
				table.insert(all, b)
				table.insert(flask, b)
			elseif p==SlackerHelper.BuffPriority.GPROT or p==SlackerHelper.BuffPriority.PROT
			then
				table.insert(all, b)
				table.insert(prot, b)
			elseif p==SlackerHelper.BuffPriority.FOOD
			then
				table.insert(all, b)
				table.insert(food, b)
			end
		end
		name = string.format("%s%s",name, offlinetext)
		name = { ["value"] = name, ["color"] = color }
		class = { ["value"] = class, ["color"] = color }
		group = { ["value"] = group }
		flask = {["value"] = flask}
		food = {["value"] = food}
		prot = {["value"] = prot}
		local row = { ["cols"] = {name, class, group, flask, food, prot} }
		if CalculateAward ~= nil
		then
			local awrd = { ["value"] = SlackerHelper.eval(CalculateAward, {class=classstr, buffs=all, timestamp=datetime}) }
			table.insert(row["cols"], 4, awrd)
		end
		table.insert(datatable, row)
	end
	local title = "Consumes for "..data:get_reason()
	local btn1 = nil
	if CalculateAward and AwardPlayer
	then
		btn1 =AwardButton(datatable, 1, 4, AwardPlayer, "Consumes")
	end
	ShowReport(title, cols, datatable, btn1)
end

local function SnapshotConsumes(data)
	if SlackerHelper.is_tbc()
	then
		return SnapshotConsumesTBC(data)
	else
		return SnapshotConsumesClassic(data)
	end
end

local function SnapshotMissingBuffs(data)
	local cols = { 
		{
			["name"] = "Name",
			["width"] = 84,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Class",
			["width"] = 60,
			["align"] = "LEFT",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Gr",
			["width"] = 20,
			["align"] = "LEFT",
			["sort"] = "dsc",
			["defaultsort"] = "asc",
		}, 
		{
			["name"] = "Druid",
			["width"] = 40,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Mage",
			["width"] = 40,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Priest",
			["width"] = 60,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Warr",
			["width"] = 40,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
		{
			["name"] = "Other",
			["width"] = 80,
			["align"] = "LEFT",
			["comparesort"] = SlackerUI.ScrollTable.NoSort,
			["DoCellUpdate"] = SlackerUI.ScrollTable.RenderBuffs
		}, 
	}
	local datatable = {}
	for player in data:get_players_iterator()
	do
		local name = player:get_name()
		local class = player:get_class()
		local classstr = class
		local group = player:get_group()
		local buffs = player:get_all_buffs_idonly()
		local offlinetext = ""
		local color = SlackerHelper.class_to_color(class)
		local druid = {}
		local mage = {}
		local priest = {}
		local warr = {}
		local other = {}
		if not player:get_online()
		then 
			offlinetext=" (off)" 
			color = SlackerHelper.class_to_color("Offline")
		end
		local buffsfiltered = {}
		for i=1,#buffs,1
		do
			local b = buffs[i]
			local p = SlackerHelper.get_buff_priority(b)
			if SlackerHelper.in_array(b, {
				21849,	-- Gift of the Wild
				21850,	-- Gift of the Wild
				24932,	-- Leader of the Pack
				9885,	-- Mark of the Wild
				1126,	-- Mark of the Wild
				9884,	-- Mark of the Wild
				5232,	-- Mark of the Wild
				8907,	-- Mark of the Wild
				5234,	-- Mark of the Wild
				6756,	-- Mark of the Wild
				24907,	-- Moonkin Aura
				28145,	-- Power of the Guardian (druid)
				467,	-- Thorns
				9910,	-- Thorns
				1075,	-- Thorns
				9756,	-- Thorns
				782,	-- Thorns
				8914,	-- Thorns
				439745,	-- Tree of Life
				407975,	-- Wild Strikes
				407977,	-- Wild Strikes
			})
			then
				table.insert(druid, b)
			elseif SlackerHelper.in_array(b, {
				10170,	-- Amplify Magic
				10169,	-- Amplify Magic
				1008,	-- Amplify Magic
				8455,	-- Amplify Magic
				23028,	-- Arcane Brilliance
				1459,	-- Arcane Intellect
				10157,	-- Arcane Intellect
				10156,	-- Arcane Intellect
				1460,	-- Arcane Intellect
				1461,	-- Arcane Intellect
				10174,	-- Dampen Magic
				604,	-- Dampen Magic
				8450,	-- Dampen Magic
				10173,	-- Dampen Magic
				8451,	-- Dampen Magic
				28142	-- Power of the Guardian (mage)
			})
			then
				table.insert(mage, b)
			elseif SlackerHelper.in_array(b, {
				14752,	-- Divine Spirit
				14819,	-- Divine Spirit
				14818,	-- Divine Spirit
				27841,	-- Divine Spirit
				6346,	-- Fear Ward
				28144,	-- Power of the Guardian (priest)
				10938,	-- Power Word: Fortitude
				1243,	-- Power Word: Fortitude
				1244,	-- Power Word: Fortitude
				1245,	-- Power Word: Fortitude
				2791,	-- Power Word: Fortitude
				10937,	-- Power Word: Fortitude
				21564,	-- Prayer of Fortitude
				21562,	-- Prayer of Fortitude
				27683,	-- Prayer of Shadow Protection
				27681,	-- Prayer of Spirit
				10958,	-- Shadow Protection
				976,	-- Shadow Protection
				10957,	-- Shadow Protection
			})
			then
				table.insert(priest, b)
			elseif SlackerHelper.in_array(b, {
				6673,	-- Battle Shout
				25289,	-- Battle Shout
				11551,	-- Battle Shout
				11549,	-- Battle Shout
				5242,	-- Battle Shout
				6192,	-- Battle Shout
				11550,	-- Battle Shout
				403215,	-- Commanding Shout
				426490,	-- Rallying Cry
				461475,	-- Valor of Azeroth
				426972, -- Vigilance
	})
			then
				table.insert(warr, b)
			elseif 
				SlackerHelper.in_array(b, {
					13159,	-- Aspect of the Pack
					20190,	-- Aspect of the Wild
					20043,	-- Aspect of the Wild
					11767,	-- Blood Pact
					6307,	-- Blood Pact
					7805,	-- Blood Pact
					7804,	-- Blood Pact
					11766,	-- Blood Pact
					11743,	-- Detect Greater Invisibility
					2970,	-- Detect Invisibility
					132,	-- Detect Lesser Invisibility
					408514,	-- Earth Shield
					11771,	-- Fire Shield
					11770,	-- Fire Shield
					8317,	-- Fire Shield
					8316,	-- Fire Shield
					2947,	-- Fire Shield
					18264,	-- Headmaster's Charge
					409580,	-- Heart of the Lion
					19480,	-- Paranoia
					28143,	-- Power of the Guardian (warlock)
					20707,	-- Soulstone Resurrection
					20765,	-- Soulstone Resurrection
					20762,	-- Soulstone Resurrection
					20764,	-- Soulstone Resurrection
					20763,	-- Soulstone Resurrection
					408696,	-- Spirit of the Alpha
					20906,	-- Trueshot Aura
					19506,	-- Trueshot Aura
					20905,	-- Trueshot Aura
					5697,	-- Unending Breath
					131,	-- Water Breathing
					546,	-- Water Walking
				}) or
				SlackerHelper.in_array(b, {
					425876,	-- Decoy
					10535,	-- Fire Resistance
					10534,	-- Fire Resistance
					8185,	-- Fire Resistance
					10477,	-- Frost Resistance
					10476,	-- Frost Resistance
					8182,	-- Frost Resistance
					8836,	-- Grace of Air
					25360,	-- Grace of Air
					10626,	-- Grace of Air
					8178,	-- Grounding
					415236,	-- Healing Rain
					6371,	-- Healing Stream
					10461,	-- Healing Stream
					10460,	-- Healing Stream
					6372,	-- Healing Stream
					5672,	-- Healing Stream
					24853,	-- Mana Spring
					5677,	-- Mana Spring
					10491,	-- Mana Spring
					10493,	-- Mana Spring
					10494,	-- Mana Spring
					10599,	-- Nature Resistance
					10598,	-- Nature Resistance
					10596,	-- Nature Resistance
					10403,	-- Stoneskin
					10405,	-- Stoneskin
					10404,	-- Stoneskin
					8157,	-- Stoneskin
					8156,	-- Stoneskin
					8072,	-- Stoneskin
					10441,	-- Strength of Earth
					25362,	-- Strength of Earth
					8163,	-- Strength of Earth
					8162,	-- Strength of Earth
					8076,	-- Strength of Earth
					25909,	-- Tranquil Air
					15110,	-- Windwall
					15108,	-- Windwall
					15109,	-- Windwall
				}) or
				SlackerHelper.in_array(b, {
					407613,	-- Beacon of Light
					407615,	-- Beacon of Light
					20217,	-- Blessing of Kings
					19977,	-- Blessing of Light
					19979,	-- Blessing of Light
					19978,	-- Blessing of Light
					19837,	-- Blessing of Might
					19836,	-- Blessing of Might
					19740,	-- Blessing of Might
					19835,	-- Blessing of Might
					19834,	-- Blessing of Might
					19838,	-- Blessing of Might
					25291,	-- Blessing of Might
					1038,	-- Blessing of Salvation
					20913,	-- Blessing of Sanctuary
					20912,	-- Blessing of Sanctuary
					20911,	-- Blessing of Sanctuary
					20914,	-- Blessing of Sanctuary
					19742,	-- Blessing of Wisdom
					25290,	-- Blessing of Wisdom
					19852,	-- Blessing of Wisdom
					19853,	-- Blessing of Wisdom
					19850,	-- Blessing of Wisdom
					19854,	-- Blessing of Wisdom
					19746,	-- Concentration Aura
					10293,	-- Devotion Aura
					10292,	-- Devotion Aura
					465,	-- Devotion Aura
					10290,	-- Devotion Aura
					643,	-- Devotion Aura
					1032,	-- Devotion Aura
					10291,	-- Devotion Aura
					19900,	-- Fire Resistance Aura
					19899,	-- Fire Resistance Aura
					19891,	-- Fire Resistance Aura
					19898,	-- Frost Resistance Aura
					19897,	-- Frost Resistance Aura
					19888,	-- Frost Resistance Aura
					25898,	-- Greater Blessing of Kings
					25890,	-- Greater Blessing of Light
					25916,	-- Greater Blessing of Might
					25782,	-- Greater Blessing of Might
					25895,	-- Greater Blessing of Salvation
					25899,	-- Greater Blessing of Sanctuary
					25918,	-- Greater Blessing of Wisdom
					25894,	-- Greater Blessing of Wisdom
					425600,	-- Horn of Lordaeron
					407880,	-- Inspiration Exemplar
					7294,	-- Retribution Aura
					10300,	-- Retribution Aura
					10298,	-- Retribution Aura
					10299,	-- Retribution Aura
					10301,	-- Retribution Aura
					20218,	-- Sanctity Aura
					19896,	-- Shadow Resistance Aura
					19895,	-- Shadow Resistance Aura
					19876,	-- Shadow Resistance Aura
	})
			then
				table.insert(other, b)
			end
		end
		name = { ["value"] = name, ["color"] = color }
		class = { ["value"] = class, ["color"] = color }
		group = { ["value"] = group }
		druid = {["value"] = druid}
		mage = {["value"] = mage}
		priest = {["value"] = priest}
		warr = {["value"] = warr}
		other = {["value"] = other}
		local row = { ["cols"] = {name, class, group, druid, mage, priest, warr, other} }
		table.insert(datatable, row)
	end
	local title = "Class Buffs for "..data:get_reason()
	ShowReport(title, cols, datatable)
end

local function DummyReport(data)
	SlackerHelper.error("NotImplemented")
end

SlackerUI.Reports.ReportData = {
	{ ["type"] = "dungeon", ["id"] = "missing-buffs", ["name"] = "Missing Buffs", ["func"] = DungeonMissingBuffs },
	{ ["type"] = "snapshot", ["id"] = "worldbuffs", ["name"] = "World Buffs", ["func"] = SnapshotWorldBuffs },
	{ ["type"] = "snapshot", ["id"] = "consumes", ["name"] = "Consumes", ["func"] = SnapshotConsumes },
	{ ["type"] = "snapshot", ["id"] = "missing-buffs", ["name"] = "Class Buffs", ["func"] = SnapshotMissingBuffs },
}
