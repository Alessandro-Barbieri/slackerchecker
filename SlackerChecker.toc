## Interface:11504
## Title: SlackerChecker
## Author: Deathbaron
## Version: 1.8
## Notes: SlackerChecker is a buff tracking addon for raids.
## OptionalDeps: DBM-Core, LibDataBroker-1.1
## SavedVariables: SlackerChecker_DB, SlackerChecker_Settings, SlackerChecker_Version

!BlizzardFix.lua

Libs\LibStub\LibStub.lua
Libs\lib-st\lib-st.xml
Libs\LibJSON-1.0\lib.xml

SlackerHelper.lua
SlackerBuffInfo.lua
SlackerInstanceInfo.lua
SlackerData.lua
SlackerUI.lua
SlackerUISettings.lua
SlackerUISettings.xml
SlackerUIReports.lua
SlackerUIReports.xml
SlackerUIMain.lua
SlackerUIMain.xml
SlackerCore.lua
